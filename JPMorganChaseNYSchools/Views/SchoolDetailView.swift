//
//  SchoolDetailView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/15/23.
//

import SwiftUI

struct SchoolDetailView: View {
    
    let school: School
    @StateObject var nySchoolsViewModel: NYSchoolsViewModel
    @State var satResultsForSchool: SchoolSAT?
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 20) {
                HStack {
                    Text(school.schoolName)
                        .font(.title)
                }
                Text("SAT Score result are available here:")
                if let satResultsForSchool = satResultsForSchool {
                    VStack(alignment: .leading) {
                        Text("Average Critical Reading Score: \(satResultsForSchool.satCriticalReading)")
                        Text("Average Math Score: \(satResultsForSchool.satMath)")
                        Text("Average Math Score: \(satResultsForSchool.satWriting)")
                    }
                }  else {
                    Text("No results to display. Our apologies.")
                }
                Text("School Overview")
                    .font(.headline)
                Text(school.overviewParagraph)
            }
            .onAppear {
                // filters the SAT Data based on the school dbn that
                // was passed via the constructor. In the event that
                // there is bad data integrity, we pick the first element return.
                let result = nySchoolsViewModel.satDataSchools.filter {$0.dbn == school.dbn}.first
                $satResultsForSchool.wrappedValue = result
            }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 8)
                .stroke(Color.black, lineWidth: 1))
            .padding()
            .shadow(radius: 5)
        }
    }
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        if let bundlePath = Bundle.main.path(forResource: "exampleSchool", ofType: "json") {
            if let jsonData = try? String(contentsOfFile: bundlePath).data(using: .utf8) {
                let school = try? JSONDecoder().decode(School.self, from: jsonData)
                if let school = school {
                    SchoolDetailView(school: school, nySchoolsViewModel: NYSchoolsViewModel())
                }
            }
        }
    }
}
