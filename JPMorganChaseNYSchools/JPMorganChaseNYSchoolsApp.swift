//
//  JPMorganChaseNYSchoolsApp.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import SwiftUI

@main
struct JPMorganChaseNYSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView(nySchoolsViewModel: NYSchoolsViewModel())
        }
    }
}
