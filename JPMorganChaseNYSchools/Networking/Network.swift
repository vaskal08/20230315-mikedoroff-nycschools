//
//  Network.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import Foundation

class Network<T: Codable> {
    
    // 1) Function that allows the Network class to make a REST API endpoint request
    //    and then passes back a Result enumeration.
        // a) The Result enumeration allows us to pass back a .success or .failure
        //    case with an associated value.
    
    func fetchData(url: URL, completion: @escaping ((Result<[T], Error>) -> Void)) async {

        // tries to make a URLRequest to the associated endpoint and has the ability to return a failure.
        // or will return the non-nil value of (Data, Response).
        do {
            
            let dataAndResponse = try await URLSession.shared.data(for: URLRequest(url: url))
        
            let decodedResponse = try JSONDecoder().decode([T].self, from: dataAndResponse.0)
            
            completion(.success(decodedResponse))
            
        } catch let err {
            completion(.failure(err))
        }
    }
}
