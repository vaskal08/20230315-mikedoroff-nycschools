//
//  SchoolView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import SwiftUI

struct SchoolView: View {    

    let school: School
    @StateObject var nySchoolsViewModel: NYSchoolsViewModel
    
    @State var shouldShowSheet = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack(alignment: .top) {
                Text(school.schoolName)
                    .bold()
                    .font(.system(size: 20))
                    .lineLimit(3, reservesSpace: true)
                Spacer()
                VStack(alignment: .trailing) {
                    Text("Start: \(school.startTime ?? "")")
                    Text("End: \(school.endTime ?? "")")
                }
            }
            Text(school.location)
                .lineLimit(3, reservesSpace: true)
            Text("Advanced Placement Courses:")
                .font(.headline)
                .bold()
            Text("* \(school.advancedplacementCourses ?? "")")
                .lineLimit(3, reservesSpace: true)
                .padding(.bottom, 6)
            Text("Academic Opportunities: ")
                .font(.headline)
                .bold()
            VStack(alignment: .leading, spacing: 4) {
                Text("* \(school.academicopportunities1 ?? "")")
                    .lineLimit(2, reservesSpace: true)
                Text("* \(school.academicopportunities2 ?? "")")
                    .lineLimit(2, reservesSpace: true)
            }
        }
        .onTapGesture {
            shouldShowSheet.toggle()
        }
        .sheet(isPresented: $shouldShowSheet, content: {
            SchoolDetailView(school: school, nySchoolsViewModel: nySchoolsViewModel)
        })
        .padding()
        .background(school.unwrappedCollegeCareerRate < 0.50 ? Color.red : school.unwrappedCollegeCareerRate < 0.80 ? Color.yellow : Color.green)
        .overlay(RoundedRectangle(cornerRadius: 8)
        .stroke(Color.black, lineWidth: 1))
        .shadow(radius: 5)
        .padding()
    }
}

struct SchoolView_Previews: PreviewProvider {
    static var previews: some View {
        if let bundlePath = Bundle.main.path(forResource: "exampleSchool", ofType: "json") {
            if let jsonData = try? String(contentsOfFile: bundlePath).data(using: .utf8) {
                let school = try? JSONDecoder().decode(School.self, from: jsonData)
                if let school = school {
                    SchoolView(school: school, nySchoolsViewModel: NYSchoolsViewModel())
                }
            }
        }
    }
}
