//
//  CollegeAcceptanceView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import SwiftUI

struct CollegeAcceptanceView: View {
    let colors = [Color(.red), Color(.yellow), Color(.green)]
    var body: some View {
        VStack(spacing: 10) {
            Text("Post Secondary Education Rates")
                .font(.system(size: 24))
            HStack {
                Text("The color of the the squares in each grid indicates the percentage of students that seek out college after high school. Please scroll horizontally and pick a school which interests you for more detail.")
            }
            .lineLimit(7, reservesSpace: true)
            LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())], spacing: 24) {
                ForEach(colors, id: \.self) { color in
                    VStack {
                        switch color {
                        case Color(.red):
                            Text("0%-50%")
                        case Color(.yellow):
                            Text("51%-80%")
                        case Color(.green):
                            Text("81%-100%") 
                        default:
                            Text("None")
                        }
                        RoundedRectangle(cornerRadius: 8)
                            .fill(color)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                }
            }
        }
        .padding(.leading, 24)
        .padding(.trailing, 24)
    }
}

struct CollegeAcceptanceView_Previews: PreviewProvider {
    static var previews: some View {
        CollegeAcceptanceView()
    }
}
