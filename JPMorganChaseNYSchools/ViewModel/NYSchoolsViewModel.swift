//
//  NYSchoolsViewModel.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import Foundation
import SwiftUI

class NYSchoolsViewModel: ObservableObject {
    
    @Published var isUpdating = false
    @Published var schools = [School]()
    @Published var satDataSchools = [SchoolSAT]()
    
    func fetchSchoolDataFromEndpoint(schoolUrl: URL, satUrl: URL, completion: @escaping (Error) -> Void) {
        
        Task {
            // Updates the Published variable that allows the HomeView
            // to show a spinner.
            DispatchQueue.main.async {
                self.isUpdating = true
            }
            // Creates a Network and passes in a Codable struct
            let schoolNetwork = Network<School>()
            await schoolNetwork.fetchData(url: schoolUrl) {result in
                switch result {
                case .success(let schools):
                    DispatchQueue.main.async {
                        self.schools = schools
                    }
                case .failure(let err):
                    completion(err)
                }
            }
            // Creates another Network object and passes in a Codable struct
            // I would like to make the fetchData request even more generic so that
            // I can pass in multiple URLs without two fetchData calls, but due to time
            // constraints I prioritized other items.
            
            let satNetwork = Network<SchoolSAT>()
            await satNetwork.fetchData(url: satUrl) {result in
                switch result {
                case .success(let sat):
                    DispatchQueue.main.async {
                        self.satDataSchools = sat
                    }
                case .failure(let err):
                    completion(err)
                }
            }
            // Update the @Published variable again so that the spinner can stop
            // showing and present the data.
            DispatchQueue.main.async {
                self.isUpdating = false
            }
        }
    }
}



struct SchoolSAT: Codable {
    let dbn: String
    let id = UUID().uuidString
    let schoolName: String
    let numSATTakers: String
    let satCriticalReading: String
    let satMath: String
    let satWriting: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case id
        case schoolName = "school_name"
        case numSATTakers = "num_of_sat_test_takers"
        case satCriticalReading = "sat_critical_reading_avg_score"
        case satMath = "sat_math_avg_score"
        case satWriting = "sat_writing_avg_score"
    }
}


