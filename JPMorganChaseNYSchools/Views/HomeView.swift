//
//  ContentView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/14/23.
//

import SwiftUI

struct HomeView: View {
    
    @StateObject var nySchoolsViewModel: NYSchoolsViewModel
    @State var errorAlert = false
    @State var errorMessage = ""
    @State var search = ""
    
    let nycBoroughs = ["MANHAT", "BROOKLYN", "BRONX", "QUEENS", "STATEN IS"]
    let nycBoroughDict = ["MANHAT": "Manhattan", "BROOKLYN": "Brooklyn", "BRONX": "The Bronx", "QUEENS": "Queens", "STATEN IS": "Staten Island"]
    
    var body: some View {
        VStack {
            Text("NYC Schools By Borough")
                .font(.system(size: 24))
            // You must show a ProgressView to the user when there is an asynchronous task occuring.
            if nySchoolsViewModel.isUpdating {
                ProgressView()
            } else {
                // Create a listen that is broken up by Borough
                List {
                    ForEach(nycBoroughs, id: \.self) { borough in
                        Section {
                            CollegeSectionView(borough: borough, nySchoolsViewModel: nySchoolsViewModel)
                        } header: {
                            Text(nycBoroughDict[borough] ?? "")
                                .font(.system(size: 24, weight: .bold))
                                .bold()
                        }
                    }
                }
                .listStyle(.plain)
            }
        }
        .alert(isPresented: $errorAlert) {
            Alert(title: Text("Network Error"), message: Text("\n\(errorMessage) \n\nPlease check your network, restart the application and try again."), dismissButton: .default(Text("Ok")))
                }
        .onAppear {
            // runs a fetch which updates our NYSchoolsViewModel and then Publishes to our arrays.
            nySchoolsViewModel.fetchSchoolDataFromEndpoint(schoolUrl: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!, satUrl: URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!) { err in
                // This completion handler fires when there was an issue with the network. We then raising a corresponding alert to alert the user there was an issue.
                errorMessage = err.localizedDescription
                errorAlert.toggle()
                
            }
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(nySchoolsViewModel: NYSchoolsViewModel())
    }
}
