//
//  SchoolSectionView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/15/23.
//

import SwiftUI

struct SchoolSectionView: View {
    let schools: [School]
    let borough: String
    let nySchoolsViewModel: NYSchoolsViewModel
    @State var isShowingDetailView = false

    var body: some View {
        CollegeAcceptanceView()
        TabView {
            ForEach(schools, id: \.id) { school in
                SchoolView(school: school, nySchoolsViewModel: nySchoolsViewModel)
                    .padding()
            }
        }
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
        .indexViewStyle(.page(backgroundDisplayMode: .always))
        .frame(height: 900)
    }
}

struct SchoolSectionView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolSectionView(schools: [], borough: "Manhatten", nySchoolsViewModel: NYSchoolsViewModel())
    }
}
