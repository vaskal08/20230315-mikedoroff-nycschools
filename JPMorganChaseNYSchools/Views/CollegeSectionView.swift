//
//  CollegeSectionView.swift
//  JPMorganChaseNYSchools
//
//  Created by Doroff, Mike on 3/15/23.
//

import SwiftUI

struct CollegeSectionView: View {
    
    let borough: String
    let nySchoolsViewModel: NYSchoolsViewModel
    
    var body: some View {
        VStack {
            CollegeAcceptanceView()
            TabView {
                ForEach(nySchoolsViewModel.schools.filter({ school in
                    if let schoolBorough = school.borough {
                        return schoolBorough.contains(borough)
                    } else {
                        return false
                    }
                }), id: \.self) { school in
                    SchoolView(school: school, nySchoolsViewModel: nySchoolsViewModel)
                }
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
            .indexViewStyle(.page(backgroundDisplayMode: .always))
            .frame(height: 550)
        }
    }
}

struct CollegeSectionView_Previews: PreviewProvider {
    static var previews: some View {
        
        CollegeSectionView(borough: "MANHATTEN", nySchoolsViewModel: NYSchoolsViewModel())
    }
}
